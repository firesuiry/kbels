# -*- coding: utf-8 -*-
import KBEngine
from KBEDebug import *
from interfaces.skillBase import skillBase

class skill_10000062(skillBase):
	def __init__(self):
		skillBase.__init__(self)

	#--------------------------------------------------------------------------------------------
	#                              Callbacks
	#--------------------------------------------------------------------------------------------

	def onUse(self,targetID,selfPos):
		#super(skill_10000062, self).onUse(targetID,selfPos)
		target = self.getEntityByID(targetID)
		if target == None:
			return
		self.causeDamage(targetID,1)


	def onKillFollower(self, targetID):
		#super().onKillFollower(targetID)
		DEBUG_MSG("skill_10000062:[%s] onKillFollower:targetID:[%s]" % (self.id, targetID))
		self.avatar.getCard(1)

	#--------------------------------------------------------------------------------------------
	#                              Effect
	#--------------------------------------------------------------------------------------------
	