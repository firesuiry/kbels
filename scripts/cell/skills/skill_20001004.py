# -*- coding: utf-8 -*-
import KBEngine
from KBEDebug import *
from interfaces.skillBase import skillBase

class skill_20001004(skillBase):
	def __init__(self):
		skillBase.__init__(self)

	#--------------------------------------------------------------------------------------------
	#                              Callbacks
	#--------------------------------------------------------------------------------------------

	def onUse(self,targetID,selfPos):
		#super(skill_20001004, self).onUse(targetID,selfPos)
		params = {
			'attAdd': 1,
			'targetEntity': self.avatar,
			'delOnRoundEnd':1
		}
		self.creatBuff(params)
		self.avatar.getArmor(1)
	#--------------------------------------------------------------------------------------------
	#                              Effect
	#--------------------------------------------------------------------------------------------
	