# -*- coding: utf-8 -*-
import KBEngine
from KBEDebug import *
from interfaces.skillBase import skillBase

class skill_10000029(skillBase):
	def __init__(self):
		skillBase.__init__(self)

	#--------------------------------------------------------------------------------------------
	#                              Callbacks
	#--------------------------------------------------------------------------------------------

	def onBeDamaged(self,srcID,sum):
		#super().onBeDamaged(srcID,sum)
		if self.silent == 1:
			return
		params = {
			'attAdd':3,
			'targetEntity':self
		}
		self.creatBuff(params)
	#--------------------------------------------------------------------------------------------
	#                              Effect
	#--------------------------------------------------------------------------------------------
	