# -*- coding: utf-8 -*-
#
"""
"""
from KBEDebug import *
import d_card_dis

from skills import *

skillsList = {}

def onInit():
	"""
	init skills.
	"""
	script = ''
	for key, datas in d_card_dis.datas.items():
		script = 'skill_' + str(key)
		try:
			DEBUG_MSG("skills.onInit::tryToLoad:[%s]" % (script))

			exec('from skills.'+script+' import '+script)
			scriptinst = eval(script)
			skillsList[key] = scriptinst
		except IOError:
			pass
	DEBUG_MSG("skills.onInit::lenOfDic:[%s] exampleNameOfScript:[%s]" % (len(skillsList),script))
		
def getSkill(skillID):
	return skillsList.get(skillID)