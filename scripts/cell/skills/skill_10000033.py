# -*- coding: utf-8 -*-
import KBEngine
from KBEDebug import *
from interfaces.skillBase import skillBase

class skill_10000033(skillBase):
	def __init__(self):
		skillBase.__init__(self)

	#--------------------------------------------------------------------------------------------
	#                              Callbacks
	#--------------------------------------------------------------------------------------------
	def onUse(self,targetID,selfPos):
		#super().onUse(targetID,selfPos)
		add = len(self.avatar.followerList)
		params = {
			'attAdd':add,
			'HPAdd':add,
			'targetEntity':self
		}
		self.creatBuff(params)

	#--------------------------------------------------------------------------------------------
	#                              Effect
	#--------------------------------------------------------------------------------------------
	