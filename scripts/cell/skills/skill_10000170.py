# -*- coding: utf-8 -*-
import KBEngine
from KBEDebug import *
from interfaces.skillBase import skillBase
import random

class skill_10000170(skillBase):
	#卡牌名称：致命射击
	#卡牌描述：随机消灭一个敌方随从。
	def __init__(self):
		skillBase.__init__(self)

	#--------------------------------------------------------------------------------------------
	#                              Callbacks
	#--------------------------------------------------------------------------------------------

	def battleCry(self,targetID,selfPos):
		targetLS = self.getFollowerAndHeroList(False)
		if len(targetLS) > 0:
			target = random.choice(targetLS)
			self.makeTargetDie(target.id)
	#--------------------------------------------------------------------------------------------
	#                              Effect
	#--------------------------------------------------------------------------------------------
	