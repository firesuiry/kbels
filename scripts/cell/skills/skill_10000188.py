# -*- coding: utf-8 -*-
import KBEngine
from KBEDebug import *
from interfaces.skillBase import skillBase

class skill_10000188(skillBase):
	#卡牌名称：秘教暗影祭司
	#卡牌描述：<b>战吼：</b>获得一个攻击力小于或等于2的敌方随从的控制权。
	def __init__(self):
		skillBase.__init__(self)

	#--------------------------------------------------------------------------------------------
	#                              Callbacks
	#--------------------------------------------------------------------------------------------
	def battleCry(self,targetID,selfPos):
		entity = self.getEntityByID(targetID)
		if entity == None:
			DEBUG_MSG("skill_10000188::battleCry:[%i].  targetID:[%s]noUse" % (self.id, targetID))
			return
		entity.changeController()

	#--------------------------------------------------------------------------------------------
	#                              Effect
	#--------------------------------------------------------------------------------------------
	