# -*- coding: utf-8 -*-
import KBEngine
from KBEDebug import *
from interfaces.skillBase import skillBase

class skill_10000064(skillBase):
	def __init__(self):
		skillBase.__init__(self)

	#--------------------------------------------------------------------------------------------
	#                              Callbacks
	#--------------------------------------------------------------------------------------------
	def onUse(self,targetID,selfPos):
		#super(skill_10000064, self).onUse(targetID,selfPos)
		params = {
			'targetEntity':self.getEntityByID(targetID)
		}
		self.creatBuff(params)

	# --------------------------------------------------------------------------------------------
	#                              Effect
	# --------------------------------------------------------------------------------------------
	# --------------------------------------------------------------------------------------------
	#                              buffEffent
	# --------------------------------------------------------------------------------------------
	def onRoundStartB(v,self,isSelf):
		if isSelf:
			self.targetEntity.die()